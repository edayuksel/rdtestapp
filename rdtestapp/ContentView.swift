//
//  ContentView.swift
//  rdtestapp
//
//  Created by Eda Yuksel on 7.09.2021.
//

import SwiftUI

struct ContentView: View {
    
    @State private var showingAlert = false
    @State private var showingInputDialog = false
    @State private var text: String = ""
    
     
    
    var body: some View {
        ZStack {
            VStack {
                
                //Welcome Text
                Text("Welcome "+text)
                    .font(.largeTitle)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                    
                
                Spacer()
                
                Text("Please enter your name")
                    .multilineTextAlignment(.center)
                    .padding()
                
                // Input Dialog Button
                Button("Enter Name", action: {
                    self.showingInputDialog = true
                })
                .foregroundColor(.white)
                .padding(20)
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
                .cornerRadius(10)
                
                Spacer()
                
                Text("Press the below botton to see the details")
                    .multilineTextAlignment(.center)
                    .padding()
                
                // See Details Button
                Button("Show Details", action: {
                    showingAlert.toggle()
                })
                .alert(isPresented: $showingAlert, content: {
                    Alert(title: Text("See Details"),
                          message: Text("You can see the details of the product"),
                          dismissButton: .default(Text("OK")))
                })
                .foregroundColor(.white)
                .padding(20)
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
                .cornerRadius(10)
                
                
                
                Spacer()
                
                    
            }
            .padding([.top, .leading, .bottom])
            
            InputDialogAlert(isShown: $showingInputDialog,
                             text: $text,
                             title: "Please enter your name",
                             onDone: {
                                text in
                             })
        }
        
        
        
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
