//
//  rdtestappApp.swift
//  rdtestapp
//
//  Created by Eda Yuksel on 7.09.2021.
//

import SwiftUI

@main
struct rdtestappApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
