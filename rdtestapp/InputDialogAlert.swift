//
//  InputDialogAlert.swift
//  rdtestapp
//
//  Created by Eda Yuksel on 19.10.2021.
//

import SwiftUI

struct InputDialogAlert: View {
    
    let screenSize = UIScreen.main.bounds
    
    @Binding var isShown: Bool
    @Binding var text: String
    var title: String = "Add Item"
    var  onDone: (String) -> Void = { _ in}
    var onCancel: () -> Void = { }
    
    var body: some View {
        ZStack {
            if isShown {
                Color.black.opacity(0.4)
                    .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            }
            VStack {
                Text(title)
                TextField("", text: $text)
                    .textFieldStyle(RoundedBorderTextFieldStyle())

                HStack {
                    Button("Done") {
                        self.isShown = false
                        self.onDone(self.text)
                        hideKeyboard()
                    }
                    .foregroundColor(.white)
                    .padding(20)
                    .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
                    .cornerRadius(10)
                    
                    Button("Cancel") {
                        self.isShown = false
                        self.onCancel()
                    }
                    .foregroundColor(.white)
                    .padding(20)
                    .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.blue/*@END_MENU_TOKEN@*/)
                    .cornerRadius(10)
                }
                
            }
            .padding()
            .frame(width: screenSize.width * 0.7, height: screenSize.height * 0.3, alignment: .center)
            .background(Color.white)
            .cornerRadius(12)
            .offset(y: isShown ? 0: screenSize.height)
            .clipped()
        }
        
    }
}

#if canImport(UIKit)
extension View {
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
#endif

struct InputDialogAlert_Previews: PreviewProvider {
    static var previews: some View {
        InputDialogAlert(isShown: .constant(true), text: .constant(""))
    }
}
